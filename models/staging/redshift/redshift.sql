with ecomm as (
  select * from {{ source('redshift', 'integration_testing_owais') }}
),

final as (
  select * from ecomm
)
select * from final
