with ecomm as (
  select * from {{ source('bigquery', 'integration_testing') }}
),

final as (
  select * from ecomm
)
select * from final
